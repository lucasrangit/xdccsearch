package com.lxbluem;

import com.google.common.collect.Lists;
import org.junit.Ignore;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class GeneralLearningTest {


    @Test
    @Ignore
    public void splitStrings() {
        String notice1 = "[EWG]: [#elitewarez] got something to offer? msg @ ..getting flooded out? computer freezing? to ignore most bot listings use /ignore -c [ewg]-[*";
        String notice2 = "\u00031 \u000311,1\u0002:: \u00035\u000314,1E\u000314L\u000314i\u00030T\u00030E\u00030\u00030W\u00030A\u000314R\u000314E\u000314Z \u000311::\u0002 \u00030Search: \u0002\u000311ONLINE\u0002 \u0002::\u0002 Join #ELITE-CHAT for Search, subscriptions, and chat!";
        String notice3 = "[EWG]-Latest: welcome to #elite-chat, p_ac! type !help to see all of my triggers.";
        String notice4 = "┌∩┐(◣_◢)┌∩┐ - #MOVIEGODS - Only channel supporting SSL XDCC \u0002-//-\u0002 All the latest releases and \u0002the fastest\u0002 PRE times on IRC \u0002-//-\u0002 Join \u0002#mg-chat\u0002 for !search/!latest \u0002-//-\u0002 Join \u0002#mg-lounge\u0002 for chat / TV Subscriptions \u0002-//-\u0002 Join \u0002#mg-help\u0002 for help";

        String[] split = notice4.split("[\\[\\], ]");
        for (String s : split) {
            String stripped = s.replaceAll("[^a-zA-Z]", "");
            System.out.println(stripped);
        }
    }

    @Test
    public void yaml_test() throws IOException {
        Yaml yaml = new Yaml();
        Map<String, Object> parentNode = new HashMap<>();
        parentNode.put("key1", "value1");

        Map<String, Object> childnode = new HashMap<>();
        childnode.put("child1", "something");
        childnode.put("child2", "something else");
        parentNode.put("key2", childnode);

        String dump;
        dump = yaml.dump(parentNode);
        yaml.dump(parentNode, new FileWriter("yamltest.yml"));
        System.out.printf(dump);

        List<String> stringList = Lists.newArrayList("A", "b", "cc", "dddd");
        dump = yaml.dump(stringList);
        System.out.printf(dump);

        Object load = yaml.load(new FileReader("yamltest.yml"));
    }

    @Test
    public void readdir() throws IOException {
        List<File> files = Arrays.asList(new File("downloads").listFiles())
                .stream()
                .filter(File::isFile)
                .peek(f -> {
                    System.out.println(f.getName() + " " + f.length());
                })
                .collect(toList());

        List<String> collect = files
                .stream()
                .map(f -> f.getName())
                .map(name -> {
                    String[] split = name.split(File.pathSeparator);
                    return split[split.length - 1];
                })
                .collect(toList());
    }

    @Test
    public void name_hashcode() {
        String name = "asd";
        long size = 123123L;

        String toBeHashed = name + size;
        int i = toBeHashed.hashCode();
        System.out.println(i);
    }

}
