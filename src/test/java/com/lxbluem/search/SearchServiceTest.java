package com.lxbluem.search;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lxbluem.irc.pack.PackInfo;
import dagger.Lazy;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class SearchServiceTest {
    @InjectMocks
    private SearchService searchService;

    @Mock
    private Lazy<SearchClient> searchClientLazy;

    @Mock
    private SearchClient searchClient;

    private ObjectMapper mapper = new ObjectMapper();


    @Before
    public void setup() throws IOException {
        when(searchClientLazy.get()).thenReturn(searchClient);
    }

    @Test
    public void search_lazy_instanciates_client() throws IOException {
        when(searchClient.search(anyString(), anyInt())).thenReturn(new PagedResult<>());

        PagedResult<PackInfo> result = searchService.searchFor("something", 0);

        verify(searchClientLazy).get();
        assertEquals(0, result.getResults().size());
    }

    @Test
    public void search_calls_clients_search_method() throws IOException {
        when(searchClient.search(anyString(), anyInt())).thenReturn(new PagedResult<>());
        String query = "something";
        int page = 0;

        searchService.searchFor(query, page);

        verify(searchClient).search(same(query), eq(page));
    }

    @Test
    public void search_lazy_instanciates_filters_out_nicknameless_packs() throws IOException {
        HashMap<String, Object> pagedResultInput = new HashMap<>();
        List<PackInfo> value = Arrays.asList(getValidPackInfo(), getValidPackInfo(), new PackInfo());
        pagedResultInput.put("results", value);
        PagedResult<PackInfo> pagedResults = mapper
                .convertValue(pagedResultInput, new TypeReference<PagedResult<PackInfo>>() {});

        when(searchClient.search(anyString(), anyInt())).thenReturn(pagedResults);

        PagedResult<PackInfo> result = searchService.searchFor("something", 0);

        verify(searchClientLazy).get();
        assertEquals(2, result.getResults().size());
    }

    private PackInfo getValidPackInfo() {
        HashMap<String, Object> packInfo = new HashMap<>();
        packInfo.put("name", "1mb testfile");
        packInfo.put("nname", "super duper alpha irc");
        packInfo.put("naddr", "192.168.99.100");
        packInfo.put("nport", 6667);
        packInfo.put("cname", "#DOWNLOAD");
        packInfo.put("uname", "mybotdcc");
        packInfo.put("n", 3);
        return mapper.convertValue(packInfo, PackInfo.class);
    }

}