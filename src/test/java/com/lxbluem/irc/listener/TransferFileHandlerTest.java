package com.lxbluem.irc.listener;

import com.google.common.collect.Lists;
import com.lxbluem.configuration.XdccConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TransferFileHandlerTest {

    @Mock
    private XdccConfiguration xdccConfiguration;

    private TransferFileHandler handler;

    @Before
    public void setUp() throws Exception {
        TransferFileNameMapper nameMapper = new TransferFileNameMapper();

        handler = new TransferFileHandler(xdccConfiguration, nameMapper);

        when(xdccConfiguration.getApplicationSettings("downloadDir", String.class))
                .thenReturn("downloads");
    }

    @Test (expected = FileAlreadyExistsException.class)
    public void file_exists_in_directory_same_size() throws NoSuchFieldException, FileAlreadyExistsException {
        File existingFile = createFileMock("file._736387652_.part", 736387652);
        ArrayList<File> files = Lists.<File>newArrayList(existingFile);

        handler.downloadsDirectory = mock(TransferFileHandler.DownloadsDirectory.class);
        when(handler.downloadsDirectory.getFileList(anyString())).thenReturn(files);

        handler.saveTo("file", 736387652);
    }

    @Test
    public void same_name_different_size_creates_new_partname() throws NoSuchFieldException, FileAlreadyExistsException {
        handler.downloadsDirectory = mock(TransferFileHandler.DownloadsDirectory.class);

        ArrayList<File> files = Lists.<File>newArrayList(
                createFileMock("file", 123123),
                createFileMock("file", 1234)
        );
        when(handler.downloadsDirectory.getFileList(anyString()))
                .thenReturn(files);

        File returnedFile = handler.saveTo("file", 736387652);
        assertEquals("downloads/file._736387652_.part", returnedFile.getPath());
    }

    @Test
    public void should_rename_file() {
        File file = createFileMock("file._736387652_.part", 736387652);
        ArrayList<File> files = Lists.<File>newArrayList(file);
        handler.downloadsDirectory = mock(TransferFileHandler.DownloadsDirectory.class);
        when(handler.downloadsDirectory.getFileList(anyString())).thenReturn(files);

        handler.finishedDownloadingFile(file);

        ArgumentCaptor<File> argumentCaptor = ArgumentCaptor.forClass(File.class);
        verify(file).renameTo(argumentCaptor.capture());
        assertEquals("downloads/file._736387652_", argumentCaptor.getValue().getPath());
    }


    private File createFileMock(String filename, long size) {
        File file1 = mock(File.class);
        when(file1.getPath()).thenReturn("downloads/" + filename);
        when(file1.getName()).thenReturn(filename);
        when(file1.length()).thenReturn(size);
        when(file1.isFile()).thenReturn(true);
        return file1;
    }

}
