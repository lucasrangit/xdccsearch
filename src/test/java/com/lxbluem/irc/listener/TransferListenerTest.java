package com.lxbluem.irc.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableSortedSet;
import com.lxbluem.irc.AdditionalChannelsProvider;
import com.lxbluem.irc.DccRequestState;
import com.lxbluem.irc.pack.PackInfo;
import org.junit.Before;
import org.junit.Test;
import org.pircbotx.Channel;
import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;
import org.pircbotx.User;
import org.pircbotx.dcc.ReceiveFileTransfer;
import org.pircbotx.exception.IrcException;
import org.pircbotx.hooks.events.IncomingFileTransferEvent;
import org.pircbotx.hooks.events.JoinEvent;
import org.pircbotx.hooks.events.NoticeEvent;
import org.pircbotx.hooks.events.TopicEvent;
import org.pircbotx.hooks.managers.ListenerManager;
import org.pircbotx.output.OutputIRC;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class TransferListenerTest {
    private static final String SELFBOTNAME = "botA";
    private static final String OTHERBOTNAME = "botB";
    public static final String CHANNEL_NAME = "#DOWNLOAD";
    public static final String NETWORK_NAME = "super duper alpha irc";
    public static final String PACK_BOT_NAME = "mybotdcc";

    private PircBotX bot = mock(PircBotX.class);
    private OutputIRC outputIRC = mock(OutputIRC.class);
    private User remoteUserBot = mock(User.class);
    private User selfUserBot = mock(User.class);
    private User otherUserBot = mock(User.class);

    private AdditionalChannelsProvider channelsProvider = mock(AdditionalChannelsProvider.class);
    private TransferFileHandler fileHandler = mock(TransferFileHandler.class);

    private TransferListener listener;

    @Before
    public void setUp() throws IOException, IrcException {
        ObjectMapper mapper = new ObjectMapper();

        HashMap<String, Object> sampleData = new HashMap<>();
        sampleData.put("name", "1mb testfile");
        sampleData.put("nname", NETWORK_NAME);
        sampleData.put("naddr", "192.168.99.100");
        sampleData.put("nport", 6667);
        sampleData.put("cname", CHANNEL_NAME);
        sampleData.put("uname", PACK_BOT_NAME);
        sampleData.put("n", 3);

        when(bot.sendIRC()).thenReturn(outputIRC);
        when(bot.getUserBot()).thenReturn(selfUserBot);
        when(bot.getNick()).thenReturn(SELFBOTNAME);
        when(otherUserBot.getNick()).thenReturn(OTHERBOTNAME);
        when(selfUserBot.getNick()).thenReturn(SELFBOTNAME);
        when(selfUserBot.getChannels()).thenReturn(ImmutableSortedSet.<Channel>of());
        when(remoteUserBot.getNick()).thenReturn(PACK_BOT_NAME);

        PackInfo packInfo = mapper.convertValue(sampleData, PackInfo.class);
        listener = new TransferListener(packInfo, SELFBOTNAME, channelsProvider, fileHandler);
    }

    @Test
    public void onJoin_should_send_privmessage() throws Exception {
        JoinEvent event = mock(JoinEvent.class);
        when(event.getBot()).thenReturn(bot);
        listener.onJoin(event);

        verify(outputIRC).message(PACK_BOT_NAME, "xdcc send #3");
        assertEquals(DccRequestState.REQUEST, listener.getStatus().state);
    }

    @Test
    public void onJoin_for_other_localbot_should_not_send_privmessage() throws Exception {
        JoinEvent event = mock(JoinEvent.class);
        when(event.getBot()).thenReturn(bot);
        when(bot.getNick()).thenReturn(OTHERBOTNAME);

        listener.onJoin(event);

        verifyZeroInteractions(outputIRC);
        assertEquals(DccRequestState.NEW, listener.getStatus().state);
    }

    @Test
    public void onTopic_for_other_localbot_should_not_make_bot_join_mentioned_channels() throws Exception {
        Channel channel = mock(Channel.class);
        when(channel.getName()).thenReturn(CHANNEL_NAME);

        String topic = "join these channels: #EXTRA_CHANNEL for searches, #HELP for help, #muh for lala";
        TopicEvent event = mock(TopicEvent.class);
        when(event.getTopic()).thenReturn(topic);
        when(event.getChannel()).thenReturn(channel);
        when(event.getBot()).thenReturn(bot);
        when(event.getUser()).thenReturn(otherUserBot);

        listener.onTopic(event);

        verifyZeroInteractions(outputIRC);
        assertEquals(DccRequestState.NEW, listener.requestState);
    }

    @Test
    public void onTopic_should_make_bot_join_mentioned_channels() throws Exception {
        String channelName = CHANNEL_NAME;
        String networkName = NETWORK_NAME;

        Channel channel = mock(Channel.class);
        when(channel.getName()).thenReturn(channelName);

        String topic = "join these channels: #EXTRA_CHANNEL for searches, #HELP for help, #muh for lala";
        TopicEvent event = mock(TopicEvent.class);
        when(event.getTopic()).thenReturn(topic);
        when(event.getChannel()).thenReturn(channel);
        when(event.getBot()).thenReturn(bot);
        when(event.getUser()).thenReturn(selfUserBot);

        // no initial channels
        Set<String> channels = new HashSet<>(Arrays.asList("#muh", "#extra_channel"));
        when(channelsProvider.getAdditionalChannels(networkName, channelName))
                .thenReturn(Collections.<String>emptySet())
                .thenReturn(channels);

        listener.onTopic(event);

        verify(channelsProvider).addAdditionalChannel(networkName, channelName, "#muh");
        verify(channelsProvider).addAdditionalChannel(networkName, channelName, "#extra_channel");
        verify(outputIRC).joinChannel("#extra_channel");
        verify(outputIRC).joinChannel("#muh");
        assertEquals(DccRequestState.NEW, listener.requestState);
    }

    @Test
    public void onTopic_should_make_bot_not_join_mentioned_but_joined_channels() throws Exception {
        String channelName = CHANNEL_NAME;
        String networkName = NETWORK_NAME;

        Channel channel = mock(Channel.class);
        when(channel.getName()).thenReturn(channelName);

        String topic = "this is topic " + CHANNEL_NAME;
        TopicEvent event = mock(TopicEvent.class);
        when(event.getTopic()).thenReturn(topic);
        when(event.getChannel()).thenReturn(channel);
        when(event.getBot()).thenReturn(bot);
        when(event.getUser()).thenReturn(selfUserBot);

        // no initial channels
        Set<String> channels = new HashSet<>(Collections.singletonList(CHANNEL_NAME));
        when(channelsProvider.getAdditionalChannels(networkName, channelName))
                .thenReturn(Collections.<String>emptySet())
                .thenReturn(channels);

        listener.onTopic(event);

        verify(channelsProvider, never()).addAdditionalChannel(networkName, channelName, CHANNEL_NAME);
        verifyZeroInteractions(outputIRC);
        assertEquals(DccRequestState.NEW, listener.requestState);
    }

    @Test
    public void onNotice_containing_channels_joins_them() throws Exception {
        String channelName = CHANNEL_NAME;
        String networkName = NETWORK_NAME;

        String notice = "please join #NEW_CHAN for transfer";
        NoticeEvent event = mock(NoticeEvent.class);
        when(event.getBot()).thenReturn(bot);
        when(event.getChannelSource()).thenReturn(SELFBOTNAME);
        when(event.getNotice()).thenReturn(notice);

        // no initial channels
        Set<String> channels = new HashSet<>(Collections.singletonList("#new_chan"));
        when(channelsProvider.getAdditionalChannels(networkName, channelName))
                .thenReturn(Collections.<String>emptySet())
                .thenReturn(channels);

        listener.requestState = DccRequestState.REQUEST;
        listener.onNotice(event);

        verify(channelsProvider).addAdditionalChannel(networkName, channelName, "#new_chan");
        verify(outputIRC).joinChannel("#new_chan");
        assertEquals(DccRequestState.REQUEST, listener.getStatus().state);
    }

    @Test
    public void onNotice_from_another_bot_containing_channels_joins_them() throws Exception {
        String channelName = CHANNEL_NAME;
        String networkName = NETWORK_NAME;

        String notice = "please join #NEW_CHAN for transfer";
        NoticeEvent event = mock(NoticeEvent.class);
        when(event.getNotice()).thenReturn(notice);
        when(event.getBot()).thenReturn(bot);

        // no initial channels
        Set<String> channels = new HashSet<>(Collections.singletonList("#new_chan"));
        when(channelsProvider.getAdditionalChannels(networkName, channelName))
                .thenReturn(Collections.<String>emptySet())
                .thenReturn(channels);

        listener.requestState = DccRequestState.REQUEST;
        listener.onNotice(event);

        verify(channelsProvider).addAdditionalChannel(networkName, channelName, "#new_chan");
        verify(outputIRC).joinChannel("#new_chan");
        assertEquals(DccRequestState.REQUEST, listener.getStatus().state);
    }

    @Test
    public void onNotice_for_another_local_bot_containing_channels_does_not_join_them() throws Exception {
        String notice = "please join #NEW_CHAN for transfer";
        NoticeEvent event = mock(NoticeEvent.class);
        when(event.getNotice()).thenReturn(notice);
        when(event.getBot()).thenReturn(bot);

        listener.requestState = DccRequestState.REQUEST;
        listener.onNotice(event);

        verifyNoMoreInteractions(outputIRC);
        assertEquals(DccRequestState.REQUEST, listener.getStatus().state);
    }

    @Test
    public void onNotice_from_another_bot_containing_channels_joins_them_during_transfer() throws Exception {
        String channelName = CHANNEL_NAME;
        String networkName = NETWORK_NAME;

        String notice = "please join #NEW_CHAN for transfer";
        NoticeEvent event = mock(NoticeEvent.class);
        when(event.getNotice()).thenReturn(notice);
        when(event.getBot()).thenReturn(bot);

        // no initial channels
        Set<String> channels = new HashSet<>(Collections.singletonList("#new_chan"));
        when(channelsProvider.getAdditionalChannels(networkName, channelName))
                .thenReturn(Collections.<String>emptySet())
                .thenReturn(channels);

        listener.requestState = DccRequestState.TRANSFER;
        listener.onNotice(event);

        verify(channelsProvider).addAdditionalChannel(networkName, channelName, "#new_chan");
        verify(outputIRC).joinChannel("#new_chan");
        assertEquals(DccRequestState.TRANSFER, listener.getStatus().state);
    }

    @Test
    public void onNotice_from_containing_resume_support() throws Exception {
        String notice = "** Sending you pack #275 (\"Some.Movie.avi\"), which is 799MB. (resume supported)";
        NoticeEvent event = mock(NoticeEvent.class);
        when(event.getNotice()).thenReturn(notice);
        when(event.getBot()).thenReturn(bot);

        listener.requestState = DccRequestState.REQUEST;
        listener.onNotice(event);

        assertTrue(listener.getStatus().resumable);
        verifyZeroInteractions(outputIRC);
    }

    @Test
    public void onNotice_from_containing_queue() throws Exception {
        String notice = "** All Slots Full, Added you to the main queue for pack 77 (\"somefile.tar\") in position 2. To Remove yourself at a later time type \"/MSG funkyBot22 XDCC REMOVE";
        NoticeEvent event = mock(NoticeEvent.class);
        when(event.getNotice()).thenReturn(notice);
        when(event.getBot()).thenReturn(bot);

        listener.requestState = DccRequestState.REQUEST;
        listener.onNotice(event);

        assertTrue(listener.requestState.equals(DccRequestState.QUEUE));
        verifyZeroInteractions(outputIRC);
    }

    @Test
    public void onNotice_from_containing_queue_info() throws Exception {
        String notice = "Queued 0h1m for \"somefile.tar\", in position 2 of 2. 3h21m or less remaining. (at 13:20)";
        NoticeEvent event = mock(NoticeEvent.class);
        when(event.getNotice()).thenReturn(notice);
        when(event.getBot()).thenReturn(bot);

        listener.requestState = DccRequestState.QUEUE;
        listener.onNotice(event);

        assertTrue(listener.requestState.equals(DccRequestState.QUEUE));
        verifyZeroInteractions(outputIRC);
    }

    @Test
    public void onNotice_from_containing_already_requested() throws Exception {
        String notice = "you already requested pack";
        NoticeEvent event = mock(NoticeEvent.class);
        when(event.getNotice()).thenReturn(notice);
        when(event.getBot()).thenReturn(bot);

        listener.requestState = DccRequestState.QUEUE;
        listener.onNotice(event);

        assertTrue(listener.requestState.equals(DccRequestState.TRANSFER_ERROR));
    }

    @Test
    public void onNotice_from_containing_download_connection_failed() throws Exception {
        String notice = "download connection failed";
        NoticeEvent event = mock(NoticeEvent.class);
        when(event.getNotice()).thenReturn(notice);
        when(event.getBot()).thenReturn(bot);

        listener.requestState = DccRequestState.QUEUE;
        listener.onNotice(event);

        assertTrue(listener.requestState.equals(DccRequestState.TRANSFER_ERROR));
    }

    @Test
    public void onNotice_from_containing_connection_refused() throws Exception {
        String notice = "download connection failed";
        NoticeEvent event = mock(NoticeEvent.class);
        when(event.getNotice()).thenReturn(notice);
        when(event.getBot()).thenReturn(bot);

        listener.requestState = DccRequestState.QUEUE;
        listener.onNotice(event);

        assertTrue(listener.requestState.equals(DccRequestState.TRANSFER_ERROR));
    }

    @Test
    public void onNotice_from_transfer_error_exits_network_and_removes_listener() throws Exception {
        String notice = "download connection failed";
        NoticeEvent event = mock(NoticeEvent.class);
        when(event.getNotice()).thenReturn(notice);
        when(event.getBot()).thenReturn(bot);

        Configuration configuration = mock(Configuration.class);
        ListenerManager listenerManager = mock(ListenerManager.class);
        when(bot.getConfiguration()).thenReturn(configuration);
        when(configuration.getListenerManager()).thenReturn(listenerManager);

        listener.requestState = DccRequestState.TRANSFER_ERROR;
        listener.onNotice(event);

        verify(outputIRC).quitServer(anyString());
        verify(listenerManager).removeListener(same(listener));
    }

    @Test
    public void onIncomingFileTransfer_starting_transfer() throws Exception {
        IncomingFileTransferEvent event = mock(IncomingFileTransferEvent.class);
        String safeFilename = "safe.name.file.txt";
        when(event.getSafeFilename()).thenReturn(safeFilename);
        when(event.getBot()).thenReturn(bot);
        when(event.getUser()).thenReturn(remoteUserBot);

        File downloadFile = mock(File.class);
        when(fileHandler.saveTo(anyString(), anyLong())).thenReturn(downloadFile);

        ReceiveFileTransfer receiveFileTransfer = mock(ReceiveFileTransfer.class);
        Configuration configuration = mock(Configuration.class);
        ListenerManager listenerManager = mock(ListenerManager.class);
        when(bot.getConfiguration()).thenReturn(configuration);
        when(configuration.getListenerManager()).thenReturn(listenerManager);
        when(event.accept(any())).thenReturn(receiveFileTransfer);
        listener.requestState = DccRequestState.REQUEST;

        listener.onIncomingFileTransfer(event);

        verify(fileHandler).saveTo(safeFilename, event.getFilesize());
        assertEquals(DccRequestState.DONE, listener.requestState);
    }

    @Test
    public void onIncomingFileTransfer_starting_transfer_from_incoming_state() throws Exception {
        IncomingFileTransferEvent event = mock(IncomingFileTransferEvent.class);
        String safeFilename = "safe.name.file.txt";
        when(event.getSafeFilename()).thenReturn(safeFilename);
        when(event.getBot()).thenReturn(bot);
        when(event.getUser()).thenReturn(remoteUserBot);

        File downloadFile = mock(File.class);
        when(fileHandler.saveTo(anyString(), anyLong())).thenReturn(downloadFile);

        ReceiveFileTransfer receiveFileTransfer = mock(ReceiveFileTransfer.class);
        Configuration configuration = mock(Configuration.class);
        ListenerManager listenerManager = mock(ListenerManager.class);
        when(bot.getConfiguration()).thenReturn(configuration);
        when(configuration.getListenerManager()).thenReturn(listenerManager);
        when(event.accept(any())).thenReturn(receiveFileTransfer);
        listener.requestState = DccRequestState.INCOMING;

        listener.onIncomingFileTransfer(event);

        verify(fileHandler).saveTo(safeFilename, event.getFilesize());
        assertEquals(DccRequestState.DONE, listener.requestState);
    }

    @Test
    public void onIncomingFileTransfer_for_another_bot_exits_method_immediately() throws Exception {
        IncomingFileTransferEvent event = mock(IncomingFileTransferEvent.class);
        when(event.getBot()).thenReturn(bot);
        when(bot.getNick()).thenReturn(OTHERBOTNAME);
        when(event.getUser()).thenReturn(remoteUserBot);

        listener.requestState = DccRequestState.REQUEST;
        listener.onIncomingFileTransfer(event);
        assertEquals(DccRequestState.REQUEST, listener.requestState);
    }

    @Test
    public void onIncomingFileTransfer_in_wrong_state_exits_method_immediately() throws Exception {
        IncomingFileTransferEvent event = mock(IncomingFileTransferEvent.class);
        when(event.getUser()).thenReturn(remoteUserBot);
        when(event.getBot()).thenReturn(bot);

        listener.requestState = DccRequestState.NEW;
        listener.onIncomingFileTransfer(event);
        assertEquals(DccRequestState.NEW, listener.requestState);
    }

    @Test
    public void onIncomingFileTransfer_no_user_in_event_method_immediately() throws Exception {
        IncomingFileTransferEvent event = mock(IncomingFileTransferEvent.class);
        when(event.getBot()).thenReturn(bot);

        listener.requestState = DccRequestState.REQUEST;
        listener.onIncomingFileTransfer(event);
        assertEquals(DccRequestState.ERROR, listener.requestState);
    }

    @Test
    public void onIncomingFileTransfer_failing_transfer_sets_request_state_to_transferFailure() throws Exception {
        IncomingFileTransferEvent event = mock(IncomingFileTransferEvent.class);
        ReceiveFileTransfer receiveFileTransfer = mock(ReceiveFileTransfer.class);
        File downloadFile = mock(File.class);

        String safeName = "safe name file";
        when(event.getSafeFilename()).thenReturn(safeName);
        when(event.getBot()).thenReturn(bot);
        when(event.getUser()).thenReturn(remoteUserBot);
        when(event.accept(any(File.class))).thenReturn(receiveFileTransfer);
        when(fileHandler.saveTo(anyString(), anyLong())).thenReturn(downloadFile);
        when(downloadFile.getName()).thenReturn(safeName);

        doThrow(new IOException("some IO exception during receiveFileTransfer.transfer()"))
                .when(receiveFileTransfer).transfer();

        listener.requestState = DccRequestState.REQUEST;
        listener.onIncomingFileTransfer(event);
        assertEquals(DccRequestState.TRANSFER_ERROR, listener.requestState);
    }

}