package com.lxbluem.irc;

import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AdditionalChannelsProviderTest {
    private AdditionalChannelsProvider provider;

    @Before
    public void setup() {
        provider = new AdditionalChannelsProvider();
    }

    @Test (expected = IllegalArgumentException.class)
    public void provider_throws_exception_on_networkname_null() {
        String channelName = "#moviegods";

        provider.getAdditionalChannels(null, channelName);
    }

    @Test (expected = IllegalArgumentException.class)
    public void provider_throws_exception_on_networkname_empty() {
        String channelName = "#moviegods";

        provider.getAdditionalChannels("", channelName);
    }

    @Test (expected = IllegalArgumentException.class)
    public void provider_throws_exception_on_channelname_null() {
        String networkName = "abjects";

        provider.getAdditionalChannels(networkName, null);
    }

    @Test (expected = IllegalArgumentException.class)
    public void provider_throws_exception_on_channelname_empty() {
        String networkName = "abjects";

        provider.getAdditionalChannels(networkName, "");
    }

    @Test (expected = IllegalArgumentException.class)
    public void provider_addChannel_throws_exception_on_channelname_null() {
        String networkName = "abjects";

        provider.addAdditionalChannel(networkName, null, "lala");
    }

    @Test (expected = IllegalArgumentException.class)
    public void provider_addChannel_throws_exception_on_channelname_empty() {
        String networkName = "abjects";

        provider.addAdditionalChannel(networkName, "", "lala");
    }

    @Test (expected = IllegalArgumentException.class)
    public void provider_addChannel_throws_exception_on_additionalDhannelname_null() {
        String channelName = "#moviegods";
        String networkName = "abjects";

        provider.addAdditionalChannel(channelName, networkName, null);
    }

    @Test (expected = IllegalArgumentException.class)
    public void provider_addChannel_throws_exception_on_networkname_null() {
        String networkName = "abjects";

        provider.addAdditionalChannel(networkName, null, null);
    }

    @Test
    public void provider_addChannel_missing_networkName() {
        String networkName = "abjects";
        String channelName = "#moviegods";
        String additionalChannelName = "#mg-chat";
        provider.addAdditionalChannel(networkName, channelName, additionalChannelName);

        Set<String> additionalChannels = provider.getAdditionalChannels(networkName, channelName);
        assertTrue(additionalChannels.contains(additionalChannelName));
        assertEquals(1, additionalChannels.size());
    }

    @Test
    public void provider_addChannel_missing_channelName() {
        String networkName = "abjects";
        String channelName = "#moviegods";
        String channelName2 = "#beast";
        String additionalChannelName = "#mg-chat";
        String additionalChannelName2 = "#mg-chat2";
        String additionalChannelName3 = "#search";
        provider.addAdditionalChannel(networkName, channelName, additionalChannelName);
        provider.addAdditionalChannel(networkName, channelName, additionalChannelName2);
        provider.addAdditionalChannel(networkName, channelName2, additionalChannelName3);

        Set<String> additionalChannels;
        additionalChannels = provider.getAdditionalChannels(networkName, channelName);
        assertTrue(additionalChannels.contains(additionalChannelName));
        assertTrue(additionalChannels.contains(additionalChannelName2));
        assertEquals(2, additionalChannels.size());

        additionalChannels = provider.getAdditionalChannels(networkName, channelName2);
        assertTrue(additionalChannels.contains(additionalChannelName3));
        assertEquals(1, additionalChannels.size());
    }

}
