package com.lxbluem.configuration;

import com.lxbluem.irc.listener.TransferFileDescription;
import org.yaml.snakeyaml.Yaml;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

public class XdccConfiguration {
    private Yaml yaml = new Yaml();
    String APPLICATION_YML = "application.yml";
    String TRANSFERS_YML = "transfers.yml";
    Reader transfersFileReader;
    Writer transfersFileWriter;

    public XdccConfiguration() {
    }

    public <T> T getApplicationSettings(String entry, Class<T> klass) throws NoSuchFieldException {
        ClassLoader loader = getClass().getClassLoader();
        InputStream configFileInputStream = loader.getResourceAsStream(APPLICATION_YML);
        Map<String, Object> allConfig = (Map<String, Object>) yaml.load(configFileInputStream);

        Object entryConfig = allConfig.get(entry);
        if (entryConfig == null) {
            throw new NoSuchElementException(
                    String.format("specified entry '%s' not found in '%s'", entry, APPLICATION_YML)
            );
        }

        String dump = yaml.dump(entryConfig);
        T t = yaml.loadAs(dump, klass);
        return t;
    }

    public List<TransferFileDescription> getSavedTransfers() {
        Yaml yaml = new Yaml();
        try {
            if (transfersFileReader == null)
                transfersFileReader = new FileReader(TRANSFERS_YML);
            return (List<TransferFileDescription>) yaml.load(transfersFileReader);
        } catch (FileNotFoundException e) {
            return Collections.emptyList();
        }
    }

    public void saveTransfers(List<TransferFileDescription> descriptions) throws IOException {
        if (transfersFileWriter == null)
            transfersFileWriter = new FileWriter(TRANSFERS_YML);

        yaml.dump(descriptions, transfersFileWriter);
    }

}
