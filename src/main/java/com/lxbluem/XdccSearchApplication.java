package com.lxbluem;

import com.lxbluem.configuration.AppModule;
import com.lxbluem.irc.TransfersController;
import com.lxbluem.search.SearchController;
import dagger.Component;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class XdccSearchApplication {

    @Singleton
    @Component (modules = {AppModule.class})
    public interface App {
        XdccSearchApplication make();
    }

    private final SearchController searchController;
    private final TransfersController transfersController;

    @Inject
    public XdccSearchApplication(SearchController searchController, TransfersController transfersController) {
        this.searchController = searchController;
        this.transfersController = transfersController;
    }

    public static void main(String[] args) throws IOException {
        createDownloadDirectory();

        App app = DaggerXdccSearchApplication_App.builder().build();
        app.make().run();
    }

    private void run() {
        WebConfiguration.setupStaticResourceLocation();
        searchController.setup();
        transfersController.setup();
    }

    private static void createDownloadDirectory() throws IOException {
        Path downloadsDir = Paths.get("downloads");
        if (!Files.isDirectory(downloadsDir)) {
            Files.createDirectory(downloadsDir);
        }

        if (!Files.isWritable(downloadsDir)) {
            throw new RuntimeException("cannot write to downloads directory");
        }
    }
}
