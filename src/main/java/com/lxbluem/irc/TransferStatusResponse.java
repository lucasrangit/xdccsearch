package com.lxbluem.irc;

import com.lxbluem.irc.pack.PackInfo;

import java.util.Set;

public class TransferStatusResponse {
    public boolean resumable;

    public boolean passive;

    public DccRequestState state;

    public long xferBytes;

    public long xferBytesTotal;

    public String localBotName;

    public PackInfo pack;

    public Set<String> additionalChannels;

    public long timestamp;
}
