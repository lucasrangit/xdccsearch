package com.lxbluem.irc;

import com.lxbluem.irc.listener.ListenerAdapterFactory;
import com.lxbluem.irc.listener.TransferListener;
import com.lxbluem.irc.pack.PackInfo;
import com.lxbluem.irc.pack.PackInfoValidator;
import com.lxbluem.irc.pack.PackValidationException;
import org.pircbotx.Configuration;
import org.pircbotx.MultiBotManager;
import org.pircbotx.PircBotX;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DccService {
    private static final Logger logger = LoggerFactory.getLogger(DccService.class);
    private final BotFactory botFactory;

    private final ListenerAdapterFactory listenerAdapterFactory;
    private MultiBotManager manager;

    Map<Integer, TransferListener> transferListenerMap;
    private PackInfoValidator packInfoValidator;

    @Inject
    public DccService(
            BotFactory botFactory,
            ListenerAdapterFactory listenerAdapterFactory,
            MultiBotManager manager,
            PackInfoValidator packInfoValidator
    ) {
        this.botFactory = botFactory;
        this.listenerAdapterFactory = listenerAdapterFactory;
        this.manager = manager;
        this.packInfoValidator = packInfoValidator;
        transferListenerMap = new HashMap<>();
    }

    public int startTransfer(PackInfo packInfo) throws PackValidationException {
        List<String> validationErrors = packInfoValidator.validate(packInfo);
        if (!validationErrors.isEmpty()) {
            logger.warn("requested invalid pack {}", packInfo);
            throw new PackValidationException(validationErrors);
        }

        PircBotX bot = botFactory.getBotForPackInfo(packInfo);
        int botId = bot.getBotId();
        Configuration configuration = bot.getConfiguration();
        TransferListener transferListener = listenerAdapterFactory
                .getTransferListener(packInfo, configuration.getName());
        transferListenerMap.put(botId, transferListener);

        configuration.getListenerManager().addListener(transferListener);
        manager.addBot(bot);
        return botId;
    }

    public void removeTransfer(int packId) {
        logger.info("about to stop pack {}", packId);

        int transferNum = getTransferNum(packId);
        if (transferNum == -1) {
            return;
        }

        PircBotX bot = manager.getBotById(transferNum);

        if (bot == null) {
            transferListenerMap.remove(transferNum);
            return;
        }

        if (bot.isConnected()) {
            String remoteBot = transferListenerMap.get(transferNum).getPack().getNickName();
            bot.send().message(remoteBot, "XDCC REMOVE");
            bot.sendIRC().quitServer();
        }
        transferListenerMap.remove(transferNum);
    }

    private int getTransferNum(long packId) {
        for (int key : transferListenerMap.keySet()) {
            PackInfo pack = transferListenerMap.get(key).getPack();
            if (pack.getPackId() == packId) {
                return key;
            }
        }
        return -1;
    }

    public List<TransferStatusResponse> getTransfers() {
        return transferListenerMap.keySet().stream()
                .sorted()
                .map(this::getTransferStatus)
                .filter(transferStatusResponse -> transferStatusResponse != null)
                .collect(Collectors.toList());
    }

    private TransferStatusResponse getTransferStatus(int botId) {
        TransferListener transferListener = transferListenerMap.get(botId);

        if (transferListener == null) {
            return null;
        }
        return transferListener.getStatus();
    }
}
