package com.lxbluem.irc.listener;

import com.lxbluem.irc.DccService;
import com.lxbluem.irc.pack.PackInfo;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.IncomingChatRequestEvent;
import org.pircbotx.hooks.events.InviteEvent;
import org.pircbotx.hooks.events.JoinEvent;
import org.pircbotx.hooks.events.NoticeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageCollectingXdccListener extends ListenerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(DccService.class);

    private PackInfo pack;

    private boolean hasJoined;

    private String notice;

    public MessageCollectingXdccListener(PackInfo packInfo) {
        logger.info("NEW LISTENER: {} with pack {}", hashCode(), packInfo);
        pack = packInfo;
    }

    @Override
    public void onJoin(JoinEvent event) throws Exception {
        if (hasJoined) {
            return;
        }

        logger.info("####   JOINED   {}  !!" + pack);

        hasJoined = true;
    }

    @Override
    public void onIncomingChatRequest(IncomingChatRequestEvent event) throws Exception {
        logger.info("#### IncomingChatRequest {}" + event);
    }

    @Override
    public void onInvite(InviteEvent event) throws Exception {
        logger.info("#### Invite {}" + event);
    }

    @Override
    public void onNotice(NoticeEvent event) throws Exception {
        notice = event.getNotice();
        logger.info("#### Notice notice  {}" + notice);
        logger.info("#### Notice message {}" + event.getMessage());
    }
}
