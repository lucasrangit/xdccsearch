package com.lxbluem.irc.listener;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class TransferFileDescription {
    @NonNull
    private String filename;
    @NonNull
    private String packFilename;
    private long size;
    private long packId;
}
