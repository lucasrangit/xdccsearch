package com.lxbluem.irc.listener;

import com.google.common.collect.ImmutableSortedSet;
import com.lxbluem.irc.AdditionalChannelsProvider;
import com.lxbluem.irc.DccRequestState;
import com.lxbluem.irc.TransferStatusResponse;
import com.lxbluem.irc.pack.PackInfo;
import org.pircbotx.Channel;
import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;
import org.pircbotx.User;
import org.pircbotx.dcc.ReceiveFileTransfer;
import org.pircbotx.hooks.Event;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.IncomingFileTransferEvent;
import org.pircbotx.hooks.events.JoinEvent;
import org.pircbotx.hooks.events.NoticeEvent;
import org.pircbotx.hooks.events.TopicEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.EnumSet;
import java.util.Set;

import static com.lxbluem.irc.DccRequestState.DONE;
import static com.lxbluem.irc.DccRequestState.ERROR;
import static com.lxbluem.irc.DccRequestState.INCOMING;
import static com.lxbluem.irc.DccRequestState.NEW;
import static com.lxbluem.irc.DccRequestState.QUEUE;
import static com.lxbluem.irc.DccRequestState.REQUEST;
import static com.lxbluem.irc.DccRequestState.TRANSFER;
import static com.lxbluem.irc.DccRequestState.TRANSFER_ERROR;

public class TransferListener extends ListenerAdapter {
    private static Logger logger = LoggerFactory.getLogger(TransferListener.class);

    private final PackInfo pack;

    private final String botName;

    private ReceiveFileTransfer transfer;

    DccRequestState requestState;

    private boolean resumeSupported;

    private final AdditionalChannelsProvider channelsProvider;
    private final TransferFileHandler fileHandler;

    private boolean isTransferPassive;

    public TransferListener(PackInfo packInfo, String botname, AdditionalChannelsProvider channelsProvider, TransferFileHandler fileHandler) {
        this.botName = botname;
        this.pack = packInfo;
        this.channelsProvider = channelsProvider;
        this.fileHandler = fileHandler;
        requestState = NEW;
        logger.info(">> {} with pack {}", botname, packInfo);
    }

    @Override
    public void onTopic(TopicEvent event) throws Exception {
        if (!event.getBot().getNick().equals(botName)) {
            logger.debug("TOPIC: message meant for bot {} not for {}", event.getBot().getNick(), botName);
            return;
        }

        String topic = event.getTopic();
        logger.debug("TOPIC: {} topic: {}", botName, topic);
        if (newChannelsMentioned(topic)) {
            joinAdditionalChannels(event);
        }
    }

    private boolean newChannelsMentioned(String topic) {
        boolean foundNewChannels = false;
        String packNetworkName = pack.getNetworkName();
        String packChannelName = pack.getChannelName();
        Set<String> additionalChannels = channelsProvider.getAdditionalChannels(packNetworkName, packChannelName);

        for (String rawTokens : topic.toLowerCase().split("[\\[\\], ]")) {
            String token = rawTokens.replaceAll("[^a-zA-Z#_-]", "");

            if (token.isEmpty()) {
                continue;
            }

            if (!token.startsWith("#")) {
                continue;
            }

            if (token.contains("help")) {
                continue;
            }

            if (token.equalsIgnoreCase(packChannelName)) {
                continue;
            }

            if (!additionalChannels.contains(token)) {
                channelsProvider.addAdditionalChannel(packNetworkName, packChannelName, token);
                foundNewChannels = true;
            }
        }
        return foundNewChannels;
    }

    private void joinAdditionalChannels(Event event) {
        Set<String> additionalChannels = channelsProvider
                .getAdditionalChannels(pack.getNetworkName(), pack.getChannelName());

        additionalChannels.forEach(c -> {
            ImmutableSortedSet<Channel> channels = event.getBot().getUserBot().getChannels();
            boolean match = channels.stream()
                    .anyMatch(channel -> additionalChannels.contains(channel.getName().toLowerCase()));

            if (match) {
                return;
            }

            logger.debug("#### {} joining ADDITIONAL CHANNEL: {}", botName, c);
            event.getBot().sendIRC().joinChannel(c);
        });
    }

    @Override
    synchronized public void onJoin(JoinEvent event) throws Exception {
        if (pack.getNickName() == null) {
            return;
        }

        if (!event.getBot().getNick().equals(botName)) {
            logger.debug("JOIN: message meant for bot {} not for {}", event.getBot().getNick(), botName);
            return;
        }

        if (requestState != NEW) {
            logger.trace("JOIN: {} not in {} state, state: {}; event: {}", botName, NEW, requestState, event);
            return;
        }

        logger.info("JOIN: {} requesting pack {{}}", botName, pack);
        event.getBot().sendIRC().message(pack.getNickName(), "xdcc send #" + pack.getPackNumber());

        requestState = REQUEST;
    }

    @Override
    public void onNotice(NoticeEvent event) throws Exception {
        if (!event.getBot().getNick().equals(botName)) {
            logger.debug("NOTICE: message meant for bot {} not for {}", event.getBot().getNick(), botName);
            return;
        }

        User user = event.getUser();
        String nick = "";
        String noticeMessage = event.getNotice().toLowerCase();

        if (noticeMessage.startsWith("ls: ")) {
            return;
        }

        if (user == null) {
            logger.debug("NOTICE: {} notice:: {}", botName, noticeMessage);
        } else {
            nick = user.getNick();
            if (nick.equals(pack.getNickName()))
                logger.info("NOTICE: {} notice:: {}: {}", botName, nick, noticeMessage);
            else
                logger.debug("NOTICE: {} notice:: {}: {}", botName, nick, noticeMessage);
        }

        if (!noticeMessage.contains("sending you pack #") && newChannelsMentioned(noticeMessage)) {
            joinAdditionalChannels(event);
        }

        if (requestState == TRANSFER_ERROR) {
            exit(event, "byyyyye!");
        }

        if (!EnumSet.of(INCOMING, REQUEST, QUEUE).contains(requestState)) {
            logger.trace("NOTICE: {} not in REQUEST, INCOMING, QUEUE state, state = {}", botName, requestState);
            return;
        }

        if (noticeMessage.contains("resume supported")) {
            resumeSupported = true;
        }

        if (noticeMessage.contains("queue for pack") || noticeMessage.contains("you already have that item queued")) {
            requestState = QUEUE;
            return;
        }

        if (noticeMessage.contains("you already requested")) {
            requestState = TRANSFER_ERROR;
        }

        if (noticeMessage.contains("download connection failed")) {
            requestState = TRANSFER_ERROR;
        }

        if (noticeMessage.contains("connection refused")) {
            requestState = TRANSFER_ERROR;
        }
    }

    @Override
    public void onIncomingFileTransfer(IncomingFileTransferEvent event) throws Exception {
        if (!event.getBot().getNick().equals(botName)) {
            logger.debug("TRANSFER: {} != event's bot name {}", botName, event.getBot().getNick());
            return;
        }

        EnumSet<DccRequestState> states = EnumSet.of(INCOMING, REQUEST);
        if (!states.contains(requestState)) {
            logger.trace("TRANSFER: {} not in {} state, state = {}", botName, states, requestState);
            return;
        }

        User user = event.getUser();
        if (user == null) {
            requestState = ERROR;
            return;
        }

        requestState = INCOMING;
        isTransferPassive = event.isPassive();

        // TODO: 16/12/29 resume
        File destination = fileHandler.saveTo(event.getSafeFilename(), pack.getPackId());

        transfer = event.accept(destination);
        requestState = TRANSFER;

        ((Runnable) () -> {
            try {
                String safeFilename = destination.getName();
                logger.info("TRANSFER: {} start TRANSFER of '{}'", botName, safeFilename);

                transfer.transfer();

                logger.info("TRANSFER: {} DONE with '{}'", botName, safeFilename);
                fileHandler.finishedDownloadingFile(destination);
                requestState = DONE;
                exit(event, "thank very much. :)");
            } catch (IOException e) {
                requestState = TRANSFER_ERROR;
                logger.error("TRANSFER: {} TRANSFER_ERROR: {}", botName, e.getMessage());
            }
        }).run();

    }

    public TransferStatusResponse getStatus() {
        TransferStatusResponse response = new TransferStatusResponse();
        response.localBotName = botName;
        response.xferBytes = 0;
        response.xferBytesTotal = pack.getSizeBytes();
        response.state = requestState;
        response.resumable = resumeSupported;
        response.pack = pack;
        response.passive = isTransferPassive;
        response.additionalChannels = channelsProvider
                .getAdditionalChannels(pack.getNetworkName(), pack.getChannelName());
        response.timestamp = Instant.now().toEpochMilli();

        if (transfer == null) {
            return response;
        }

        response.xferBytes = transfer.getBytesTransfered();
        response.xferBytesTotal = transfer.getFileSize();

        return response;
    }

    public PackInfo getPack() {
        return pack;
    }

    private void exit(Event event, String msg) {
        PircBotX bot = event.getBot();
        bot.sendIRC().quitServer(msg);
        Configuration configuration = bot.getConfiguration();
        configuration.getListenerManager().removeListener(this);
        logger.info("{} EXITING", botName);
    }
}
