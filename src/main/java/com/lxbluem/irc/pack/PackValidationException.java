package com.lxbluem.irc.pack;

import java.util.List;

public class PackValidationException extends Exception {
    private List<String> validationErrors;

    public PackValidationException(List<String> validationErrors) {
        this.validationErrors = validationErrors;
    }

    public List<String> getValidationErrors() {
        return validationErrors;
    }
}
